package zombieversity.controller.core;

/**
 * 
 * Start the application.
 *
 */
public final class GameLauncher {

    private GameLauncher() {
    }

    /**
     * Main method of application.
     * 
     * @param args parameters
     */
    public static void main(final String[] args) {
        Game.main(args);
    }

}
