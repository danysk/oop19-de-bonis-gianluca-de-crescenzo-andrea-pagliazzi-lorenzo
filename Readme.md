# Zombieversity

Zombieversity is a third person top-down shooter/survival. Your goal is to survive zombies hordes, as much as you can. The longer you stay alive the higher will be your score, challenge the community in the leaderboard and try to be the best.  

## Instruction

Execute the jar, be ready to play and start!


W,A,S,D and arrows keys to move the player, mouse to aim and shoot.

## Downloads
[Executable JAR](https://bitbucket.org/sofiaponi/oop19-zombiegame/raw/ae68b4a50c5e14c9131537fe35cc7ed7af99472c/report-jar/oop-zombiegame.jar)
[Report](https://bitbucket.org/sofiaponi/oop19-zombiegame/raw/ae68b4a50c5e14c9131537fe35cc7ed7af99472c/report-jar/Relazione.pdf)

## Contributing
Gianluca De Bonis, Andrea De Crescenzo, Lorenzo M. Pagliazzi, Sofia Poni.

## License
Zombieversity & co. 